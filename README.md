# README #
Snake.py: Corresponds to the challenge 'Algorithm and data structures'
Reports.py: Corresponds to the challenge 'Data analytics':
    The dependencies used are: 
        - pandas: Dependency to apply data filtering
        - re: Dependency to apply regular expressions
    How-To-Use:
        - Execute Reports.py: 'py Reports.py'
        - Write the chosen option. Example: TOP_KO. 
          It is possible to use multiples choises by using commas. Example: TOP_OK, TOP_KO 