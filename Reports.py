import pandas as pd
import re

regexV1 = '([(\d\.)]+) - - \[(.*?)\] "(.*?)" (\d+) (\d+) "(.*?)" "(.*?)"'
regexV2 = '([(\d\.)]+) - - \[(.*?)\] "(.*?)" (\d+) - "(.*?)" "(.*?)"'
regexV3 = '([(\d\.)]+) - - \[(.*?)\] "(.*?)" (\d+) (\d+) "-" "(.*)'


def initDataframe():
    global dateFrame
    f = open("apache_logs.txt", "r")
    ips = []
    statusCodes = []
    sizes = []
    pages = []
    for line in f:
        if re.match(regexV1, line) != None:
            groups = re.match(regexV1, line).groups()
            ips.append(groups[0])
            statusCodes.append(int(groups[3]))
            sizes.append(int(groups[4]))
            pages.append(groups[5])
        elif re.match(regexV2, line) != None:
            ips.append(groups[0])
            statusCodes.append(int(groups[3]))
            sizes.append(int(groups[4]))
            pages.append(groups[5])
        else:
            ips.append(groups[0])
            statusCodes.append(int(groups[3]))
            sizes.append(int(groups[4]))
            pages.append(groups[5])
    f.close()
    data = {'ip': ips,
            'status_code': statusCodes,
            "size": sizes,
            'page': pages}
    dateFrame = pd.DataFrame(data)


def getTopPages():
    topPages = dateFrame[(dateFrame.page != '-')].groupby(['page']).size().sort_values(ascending=False).reset_index(
        name='count').head(10)
    print('\n>>> TOP_10: ')
    print(topPages)


def getPercentageOK():
    percentageOK = (dateFrame[(dateFrame.status_code >= 200) & (
            dateFrame.status_code < 400)].count().ip / dateFrame.count().ip) * 100
    print('\n>>> TOP_OK: ')
    print(percentageOK)


def getPercentageKO():
    topKO = dateFrame[dateFrame.status_code >= 400]
    percentageKO = (topKO.count().ip / dateFrame.count().ip) * 100
    print('\n>>> TOP_KO: ')
    print(percentageKO)


def getTopUnsuccessful():
    topUnsuccessful = dateFrame[(dateFrame.status_code >= 400) & (dateFrame.page != '-')].groupby(
        ['page']).size().sort_values(
        ascending=False).reset_index(name='count').head(10)
    print('\n- TOP_UNSUCCES: ')
    print(topUnsuccessful)


def getTopIps():
    topIps = dateFrame[(dateFrame.page != '-')].groupby(['ip', 'page']).size().sort_values(ascending=False).reset_index(
        name='count').drop_duplicates(subset='page').head(10)
    print('\n>>> TOP_IPS: ')
    print(topIps)

def generateReport(option):
    switcher = {
        'TOP_10': getTopPages,
        'TOP_OK': getPercentageOK,
        'TOP_KO': getPercentageKO,
        'TOP_UNSUCCES': getTopUnsuccessful,
        'TOP_IPS': getTopIps
    }
    func = switcher.get(option, lambda: 'Invalid')
    return func()

if __name__ == '__main__':
    initDataframe()

    print('Which report do you want generate?')
    print('- Options: TOP_10, TOP_OK, TOP_KO, TOP_UNSUCCES, TOP_IPS')
    options = input()

    for option in options.split(','):
        generateReport(option.strip().upper())
