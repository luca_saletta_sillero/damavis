

def numberOfAvailableDifferentPaths(board, snake, depth):
    print("Board: " + str(board))
    print("Snake: " + str(snake))
    print("Depth: " + str(depth))

    ## Inputs validations
    validsInputs = checkBoard(board, True)
    validsInputs = checkSnake(board, snake, validsInputs)
    validsInputs = checkDepth(depth, validsInputs)

    if validsInputs:
        adjacentCells = getAdjacentCells(board, snake[0], snake)
        adjacentCells, totalPaths = snakeForward(adjacentCells, board, snake, 1, depth, 0)

        print("Output: " + str(totalPaths))

        return totalPaths


def checkDepth(depth, validsInputs):
    allowedDepthRange = range(1, 21)
    if depth not in allowedDepthRange:
        validsInputs = False
        print("Input Error: Depth range")
    return validsInputs


def checkSnake(board, snake, validsInputs):
    snakeAllowedRange = range(3, 8)
    if len(snake) not in snakeAllowedRange:
        validsInputs = False
        print("Input Error: Snake length")
    for item in snake:
        if len(item) != 2:
            validsInputs = False
            print("Input Error: Snake item length")
            break
    for i in range(0, len(snake)):
        for j in range(0, 2):
            itemAllowedRange = range(0, board[j])
            if snake[i][j] not in itemAllowedRange:
                print("Input Error: Snake item range")
                break
    return validsInputs


def checkBoard(board, validsInputs):
    if len(board) != 2:
        validsInputs = False
        print("Input Error: Board length")
    boardAllowedRange = range(1, 11)
    for index in board:
        if index not in boardAllowedRange:
            validsInputs = False
            print("Input Error: Board range")
            break
    return validsInputs


def snakeForward(adjacentCells, board, snake, currentDepth, depth, totalPaths):
        for way in adjacentCells:

            ## Generating new snake array
            newSnake = updateSnake(snake, way)

            ## Obtain adjacent cells of new head
            adjacentCellsTemp = getAdjacentCells(board, newSnake[0], newSnake)

            ## Check exit restrictions
            if len(adjacentCellsTemp) > 0 and currentDepth < depth:
                ## Calculate next movement
                adjacentCellsTemp, totalPaths = snakeForward(adjacentCellsTemp, board, newSnake, currentDepth + 1, depth, totalPaths)
            else:
                ## Increment total paths
                totalPaths = totalPaths + 1

                ## Remove evaluate path
                adjacentCells.remove(way)

                ## Calcule other paths
                adjacentCells, totalPaths = snakeForward(adjacentCells, board, newSnake, currentDepth + 1, depth,
                                                         totalPaths)
                return adjacentCells, totalPaths
        return adjacentCells, totalPaths



def updateSnake(snake, way):
    snakeTemp = snake.copy()
    snakeTemp[0] = way
    for index in range(0, len(snake) - 1):
        snakeTemp[index + 1] = snake[index]
    return snakeTemp


def getAdjacentCells(board, position, snake):
    adjacentCells = list()
    rowRange = range(0, board[0])
    columRange = range(0, board[1])

    # The body of the snake are not allowed cells, except the head.
    notAllowedCells = snake.copy()
    notAllowedCells.remove(snake[len(snake)-1])

    for dx in range(-1, 2):
        for dy in range(-1, 2):
            row = position[0] + dx
            colum = position[1] + dy
            adjacentCell = [row, colum]

            ## Check:
            ## -- row are inside the board
            ## -- colum are inside the board
            ## -- Removed diagonal movement
            ## -- Removed adjacent cells that are not allowed
            if row in rowRange and colum in columRange and abs(dx) != abs(dy) and adjacentCell not in notAllowedCells:
                adjacentCells.append(adjacentCell)

    return adjacentCells


if __name__ == '__main__':

    board = [4, 3]
    snake = [[2, 2], [3, 2], [3, 1], [3, 0], [2, 0], [1, 0], [0, 0]]
    depth = 3
    numberOfAvailableDifferentPaths(board, snake, depth)

    board = [2, 3]
    snake = [[0, 2], [0, 1], [0, 0], [1, 0], [1, 1], [1, 2]]
    depth = 10

    numberOfAvailableDifferentPaths(board, snake, depth)

    board = [10, 10]
    snake = [[5, 5], [5, 4], [4, 4], [4, 5]]
    depth = 4

    numberOfAvailableDifferentPaths(board, snake, depth)